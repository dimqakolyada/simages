/* global chrome */

const textarea = document.getElementById('textarea');
const folderName = document.getElementById('folderName');
const maxPhotoCounter = document.getElementById('maxPhotoCounter');
const form = document.getElementById('form');
const progress = document.getElementById('progress');

let taskTotal = 0;

async function loadSettings() {
  const result = await chrome.storage.local.get(['folder', 'sids', 'task_left', 'task_total', 'photoCounter']);
  folderName.value = result.folder || '';
  maxPhotoCounter.value = result.photoCounter || '';
  textarea.value = result.sids || '';
  form.hidden = (result.task_left > 0);
  progress.hidden = !((result.task_left > 0));
  taskTotal = result.task_total || 0;
}

loadSettings();

chrome.storage.onChanged.addListener((changes) => {
  if (changes.task_left) {
    const percentage = 100 - Math.round((changes.task_left.newValue / taskTotal) * 100);
    progress.children[0].textContent = `${percentage}%`;
    progress.children[0].style.width = `${percentage}%`;
    form.hidden = (changes.task_left.newValue > 0);
    progress.hidden = !(changes.task_left.newValue > 0);
  }
  if (changes.task_total) {
    taskTotal = changes.task_total.newValue;
  }
});
document.addEventListener('DOMContentLoaded', () => {
  const getSidsButton = document.getElementById('getSidsButton');
  getSidsButton.addEventListener('click', () => {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      const activeTab = tabs[0];
      if (activeTab) {
        chrome.runtime.sendMessage({
          message: 'getSids',
          tabId: activeTab.id,
        });
      }
    });
  });

  const downloadImagesButton = document.getElementById('downloadImages');
  downloadImagesButton.addEventListener('click', () => {
    const re = /\d{6,8}/g;
    const sids = [[]];
    let setCounter = 0;
    let sid = re.exec(textarea.value);
    while (sid) {
      if (sids[setCounter].length < 50) {
        sids[setCounter].push(sid[0]); // Используем sid[0] для получения строки
      } else {
        setCounter += 1;
        sids[setCounter] = [sid[0]]; // Используем sid[0]
      }
      sid = re.exec(textarea.value);
    }
    const eachSidInFolder = document.getElementById('eachSidInFolder').checked;
    chrome.runtime.sendMessage({
      message: 'getSidsImages',
      sids,
      folder: folderName.value,
      photoCounter: maxPhotoCounter.value,
      eachSidInFolder,
    });
  });
});

folderName.addEventListener('change', (element) => {
  chrome.storage.local.set({ folder: element.target.value });
});

maxPhotoCounter.addEventListener('change', (element) => {
  chrome.storage.local.set({ photoCounter: element.target.value });
});

textarea.addEventListener('change', (element) => {
  chrome.storage.local.set({ sids: element.target.value });
});

chrome.runtime.onMessage.addListener((message) => {
  if (message.message === 'sids') {
    textarea.value = message.data;
  }
});
