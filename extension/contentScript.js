/* global chrome */

async function requestItemInfo() {
  const items = document.querySelectorAll('.catalog__list>.catalog__item, .products>.product, .cart-items-table>.catalog__item, .catalog>.catalog__item');
  const itemIdsSet = [[]];
  let arrayCounter = 0;
  const arrayLimit = 50;

  items.forEach((item, index) => {
    arrayCounter = Math.trunc(index + 1 / arrayLimit);
    if (typeof itemIdsSet[arrayCounter] === 'undefined') {
      itemIdsSet[arrayCounter] = [];
    }
    itemIdsSet[arrayCounter].push(item.dataset.key);
  });

  const requests = itemIdsSet.map((itemIds) => fetch(`https://www.sima-land.ru/api/v3/item/?simages&id=${itemIds.toString()}`));
  const responses = await Promise.all(requests);
  const responseBodyPromises = responses.map((response) => response.json());
  const responseBodys = await Promise.all(responseBodyPromises);
  return responseBodys;
}

async function getSids() {
  const items = (await requestItemInfo()).reduce(
    (acc, itemsResponse) => acc.concat(itemsResponse.items),
    [],
  );
  const sids = items.map((item) => item.sid).join('\n');
  chrome.runtime.sendMessage({ message: 'sids', data: sids, tabId: chrome.runtime.id });
  chrome.storage.local.set({ sids });
}

getSids();
