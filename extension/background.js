/* global chrome */
let downloads = [];
const photoUrls = {};

function downloadImages(items) {
  if (items.length === 0) {
    return true;
  }

  return items.forEach((item) => {
    const filename = photoUrls.eachSidInFolder
      ? `${photoUrls.folder || 'simages'}/${item.sid}/${item.sid}_${item.index}.jpg`
      : `${photoUrls.folder || 'simages'}/${item.sid}_${item.index}.jpg`;

    chrome.downloads.download({
      url: item.url,
      filename,
    }, (downloadId) => {
      downloads.push(downloadId);
    });
  });
}

chrome.runtime.onMessage.addListener(async (message) => {
  if (message.message === 'getSids') {
    try {
      await chrome.scripting.executeScript({
        target: { tabId: message.tabId }, // Убедитесь, что вы отправляете tabId в сообщении
        files: ['contentScript.js'],
      });
    } catch (error) {
      console.error('Error executing script:', error);
    }
  }

  if (message.message && message.message === 'getSidsImages') {
    photoUrls.folder = message.folder;
    photoUrls.eachSidInFolder = message.eachSidInFolder;

    photoUrls.data = await message.sids.reduce(async (acc, sidsSet) => {
      const itemsInfoRequest = await fetch(`https://www.sima-land.ru/api/v3/item/?simages&sid=${sidsSet.join(',')}`, {
        method: 'GET',
        headers: {
          'User-Agent': 'Simages',
        },
      });
      const itemsInfo = (await itemsInfoRequest.json()).items;
      const photos = itemsInfo.reduce((accumulator, item) => {
        let urls = item.photos.map((photo, index) => ({
          sid: item.sid,
          index,
          url: `${photo.url_part}700-nw.jpg`,
        }));
        if (message.photoCounter) {
          urls = urls.slice(0, +message.photoCounter);
        }
        return accumulator.concat(urls);
      }, []);
      const newAcc = await acc;
      return newAcc.concat(photos);
    }, Promise.resolve([])); // Начальное значение теперь - Promise.resolve([])

    chrome.storage.local.set({ task_total: photoUrls.data.length });
    downloadImages(photoUrls.data.slice(0, 1));
    photoUrls.data = photoUrls.data.slice(1);
  }
  return true;
});

chrome.downloads.onChanged.addListener((downloadDelta) => {
  if (downloadDelta.state && downloadDelta.state.current === 'complete') {
    downloads = downloads.filter((downloadId) => downloadId !== downloadDelta.id);
    if (downloads.length < 10) {
      downloadImages(photoUrls.data.slice(0, 1));
      photoUrls.data = photoUrls.data.slice(1);
      chrome.storage.local.set({ task_left: photoUrls.data.length });
    }
  }
});
